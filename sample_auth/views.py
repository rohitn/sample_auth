from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.exceptions import ObjectDoesNotExist
from sample_auth.portal.models import UserProfile
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.core import serializers
from django.core.mail import send_mail
import datetime, random, sha

def main_page(request):
  return render_to_response('index.html')

def logout_page(request):
  """
  Log users out and re-direct them to the main page.
  """
  logout(request)
  return HttpResponseRedirect('/')

def get_or_create_profile(user):
  try:
    profile = user.get_profile()
  except ObjectDoesNotExist:
    profile = UserProfile(user=user, url="http://example.com",
                          home_address="123 adjf way",
                          phone_number=12343,
                          accepted_eula=True)
    profile.save()
  return profile

def register(request, user_id=None):
  user = User.objects.get(pk=user_id)
  user.userprofile = get_or_create_profile(user)
  data = serializers.serialize("json", UserProfile.objects.all())
  return HttpResponse(data)

def RegistrationSetup(request):
  salt = sha.new(str(random.random())).hexdigest()[:5]
  activation_key = sha.new(salt+new_user.username).hexdigest()
  key_expires = datetime.datetime.today() + datetime.timedelta(2)
   # Send an email with the confirmation link                                                                                                                      
  email_subject = 'Your new example.com account confirmation'
  email_body = "Hello, %s, and thanks for signing up for an example.com account!\n\nTo activate your account, click this link within 48 hours:\n\nhttp://example.com/accounts/confirm/%s" % (
                new_user.username,
                new_profile.activation_key)
  send_mail(email_subject, email_body,'accounts@example.com',
                      [new_user.email])
  return render_to_response('register.html', {'created': True})

def confirm(request, activation_key):
  if request.user.is_authenticated():
      return render_to_response('confirm.html', {'has_account': True})
  user_profile = get_object_or_404(UserProfile,
                                   activation_key=activation_key)
  if user_profile.key_expires < datetime.datetime.today():
      return render_to_response('confirm.html', {'expired': True})
  user_account = user_profile.user
  user_account.is_active = True
  user_account.save()
  return render_to_response('confirm.html', {'success': True})