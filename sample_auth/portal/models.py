from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

# Create your models here.
class UserProfile(models.Model):
  user = models.OneToOneField(User)
  url = models.URLField()
  home_address = models.TextField()
  phone_number = models.IntegerField()
  accepted_eula = models.BooleanField()
  favorite_animal = models.CharField(max_length=20, default="asfd")

#alternate userprofile class for examples
"""class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(maxlength=40)
    key_expires = models.DateTimeField()"""

def create_user_profile(sender, instance, created, **kwargs):
  if created:
    UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)