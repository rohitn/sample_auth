from django.conf.urls.defaults import *
from sample_auth.portal.views import *

urlpatterns = patterns('',

    # Main web portal entrance.
    (r'^$', portal_main_page),

)